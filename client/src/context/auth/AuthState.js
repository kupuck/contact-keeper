import React, {useReducer} from 'react';
import axios from 'axios';
import AuthContext from '../auth/authContext';
import authReducer from '../auth/authReducer';
import setAuthToken from '../../utils/setAuthToken';
import {
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  USER_LOADED,
  AUTH_ERROR,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT,
  CLEAR_ERRORS
} from '../types';

const AuthState = props => {
  const initialState = {
    token: localStorage.getItem('token'),
    isAuth: null,
    user: null,
    loading: true,
    error: null
  };
  const [state, dispatch] = useReducer(authReducer, initialState);

  const register = async data => {
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    try {
      const response = await axios.post('/api/users', data, config);
      dispatch({ type: REGISTER_SUCCESS, payload: response.data });
      loadUser();
    } catch (error) {
      dispatch({ type: REGISTER_FAIL, payload: error.response.data.msg });
    }
  };

  const loadUser = async () => {
    if (localStorage.token) {
      setAuthToken(localStorage.token);
    }
    try {
      const response = await axios.get('/api/auth');
      dispatch({ type: USER_LOADED, payload: response.data });
    } catch (error) {
      dispatch({ type: LOGIN_FAIL });
    }
  };

  const login = async data => {
    const config = {
      headers: { 'Content-Type': 'application/json' }
    }
    try {
      const response = await axios.post('/api/auth', data, config);
      dispatch({ type: LOGIN_SUCCESS, payload: response.data });
      loadUser();
    } catch (error) {
      dispatch({ type: LOGIN_FAIL, payload: error.response.data.msg });
    }
  };

  const logout = () => dispatch({ type: LOGOUT });

  const clearErrors = () => dispatch({ type: CLEAR_ERRORS });

  return (
    <AuthContext.Provider value={{
      token: state.token,
      isAuth: state.isAuth,
      user: state.user,
      loading: state.loading,
      error: state.error,
      register,
      loadUser,
      login,
      logout,
      clearErrors
    }}>
      {props.children}
    </AuthContext.Provider>
  );
};

export default AuthState;